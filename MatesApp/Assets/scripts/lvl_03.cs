﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class lvl_03 : MonoBehaviour {

    [SerializeField]
    Text respostaA, respostaB;
    public GameObject restart, nextlevel;
    public GameObject block_screen, p_help;
    private float massa, gravetat, força_h, fregament,acceleracio,espai;
    [SerializeField]
    Animator animC;

    void Start()
    {
        massa = 10;
        força_h = 40;
        fregament = 1;
        gravetat = 9.8f;
        StartCoroutine(IE_anim());
    }

    public void Comparar()
    {
        string resultatAJugador = respostaA.text;
        string resultatBJugador = respostaB.text;

        if (resultatAJugador == CalcularAcceleracio() && resultatBJugador == CalcularEspai())
        {
            block_screen.SetActive(true);
            nextlevel.SetActive(true);
        }
        else if (resultatAJugador == CalcularAcceleracio())
        {
            Debug.Log("A GOOD");
            respostaA.GetComponentInParent<InputField>().enabled = false;
        }
        else if (resultatBJugador == CalcularEspai())
        {
            Debug.Log("B GOOD");
            respostaB.GetComponentInParent<InputField>().enabled = false;
        }

    }

    string CalcularAcceleracio()
    {
        float FN,FR;
        FN = massa * gravetat;
        FR = fregament * gravetat;
        //EFX = FN-FR = massa * acceleracio;
        //aillem la acceleracio
        acceleracio = (força_h - gravetat) / massa;
        double respostaA = System.Math.Round(acceleracio, 1);
        Debug.Log("acceleracio" + respostaA);
        return respostaA.ToString();
    }
    string CalcularEspai()
    {
        acceleracio = 3;
        espai = 0.5f * acceleracio * 5 * 5;
        double respostaB = System.Math.Round(espai, 1);
        Debug.Log("velocitat " + respostaB);
        return respostaB.ToString();
    }
    public void Restart()
    {
        SceneManager.LoadScene("lvl_03");
    }
    public void NextLevel()
    {
        SceneManager.LoadScene("lvl_04");
    }
    IEnumerator IE_anim()
    {
        yield return new WaitForSeconds(6.5f);
        animC.Play("lvl_03", -1, 0f);
        StartCoroutine(IE_anim());
    }
    public void OpenHELP()
    {
        if (!p_help.activeInHierarchy)
        {
            p_help.SetActive(true);
        }
        else
        {
            p_help.SetActive(false);
        }
    }
}
