﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class lvl_05 : MonoBehaviour {


    [SerializeField]
    Text respostaA, respostaB,respostaC1,respostaC2;
    public GameObject restart, nextlevel;
    public GameObject block_screen, p_help;
    private float massa1, massa2;
    Vector2 velocitat_i1, velocitat_i2, velocitat;
    public string SA, SC1, SC2;
    [SerializeField]
    Animator animC;

    void Start()
    {
        massa1 = 2;
        massa2 = 4;
        velocitat_i1 = new Vector2(2, -3);
        velocitat_i2 = new Vector2(-3, -3);
        velocitat = new Vector2(-3, -2);
        CalculaVelocitatA();
        CalculaC1();
        Calcula2();
        PreguntaB();
        StartCoroutine(IE_anim());
    }

    public void Comparar()
    {
        string resultatAJugador = respostaA.text;
        string resultatBJugador = respostaB.text;
        string resultatC1Jugador = respostaC1.text;
        string resultatC2Jugador = respostaC2.text;

        if (resultatAJugador == CalculaVelocitatA() && resultatBJugador == PreguntaB() && resultatC1Jugador ==CalculaC1() && resultatC2Jugador == Calcula2())
        {
            block_screen.SetActive(true);
            nextlevel.SetActive(true);
        }
        if (resultatAJugador == CalculaVelocitatA())
        {
            Debug.Log("A GOOD");
            respostaA.GetComponentInParent<InputField>().enabled = false;
        }
         if (resultatBJugador == PreguntaB())
        {
            Debug.Log("B GOOD");
            respostaB.GetComponentInParent<InputField>().enabled = false;
        }
         if (resultatC1Jugador == CalculaC1())
        {
            Debug.Log("C1 GOOD");
            respostaC1.GetComponentInParent<InputField>().enabled = false;
        }
         if (resultatC2Jugador == Calcula2())
        {
            Debug.Log("C2 GOOD");
            respostaC2.GetComponentInParent<InputField>().enabled = false;
        }

    }

    string CalculaVelocitatA()
    {
        //P=m*v;
        Vector2 Pi,Pf,v2;
        Pi = (massa1 * (velocitat_i1)) + (massa2*(velocitat_i2));
        Pf = Pi - (2*velocitat);
        v2 = Pf / 4;
        //Debug.Log("pi= " + Pi + " pf= " + Pf + " v2= " + v2);
        SA = v2.ToString();
        Debug.Log(SA);
        return SA;
    }
    string PreguntaB()
    {
        string solucioB = "Elastic";
        return solucioB;
    }
    string CalculaC1()
    {
        float EC1;
        EC1= 0.5f*massa1*13 - 0.5f * massa1 * 13;
        SC1 = EC1.ToString();
        Debug.Log("EC1= " + SC1);
        return SC1;
    }
    string Calcula2()
    {
        float EC2;
        EC2 = 0.5f * massa2 *18 - 0.5f * massa2 * 12.5f;
        SC2 = EC2.ToString();
        Debug.Log("EC2= " + SC2);
        return SC2;
    }
    public void Restart()
    {
        SceneManager.LoadScene("lvl_05");
    }
    public void NextLevel()
    {
        SceneManager.LoadScene("credits");
    }
    IEnumerator IE_anim()
    {
        yield return new WaitForSeconds(6.5f);
        animC.Play("lvl_05", -1, 0f);
        StartCoroutine(IE_anim());
    }
    public void OpenHELP()
    {
        if (!p_help.activeInHierarchy)
        {
            p_help.SetActive(true);
        }
        else
        {
            p_help.SetActive(false);
        }
    }
}
