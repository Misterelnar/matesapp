﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class lvl_02 : MonoBehaviour {
    [SerializeField]
    Text respostaA, respostab;
    public GameObject restart, nextlevel, p_help;
    public GameObject block_screen;
    private float gravetat, temps, altura,velocitat;
    [SerializeField]
    Animator animC;

	void Start () {
        gravetat = 10;
        altura = 1.5f;
        StartCoroutine(IE_anim());
    }

    public void Comparar()
    {
        string resultatAJugador = respostaA.text;
        string resultatBJugador = respostab.text;

        if(resultatAJugador==CalculaTemps() && resultatBJugador == CalculaVelocitat())
        {
            block_screen.SetActive(true);
            nextlevel.SetActive(true);
        }
        else if(resultatAJugador==CalculaTemps())
        {
            Debug.Log("A GOOD");
            respostaA.GetComponentInParent<InputField>().enabled = false;
        }
        else if(resultatBJugador == CalculaVelocitat())
        {
            Debug.Log("B GOOD");
            respostab.GetComponentInParent<InputField>().enabled = false;
        }

    }

    string CalculaTemps()
    {
        //y=(H-(g*t2))/2
        //aillemt la t
        temps = (-2 * -altura)/10;
        temps = Mathf.Sqrt(temps);
        double respostaA = System.Math.Round(temps, 1);
        Debug.Log("temps " + respostaA);
        return respostaA.ToString();
    }
    string CalculaVelocitat()
    {
        temps=0.5f;
        velocitat = gravetat * temps;
        double respostaB = System.Math.Round(velocitat, 1);
        Debug.Log("velocitat " + velocitat);
        return respostaB.ToString();
    }
    public void Restart()
    {
        SceneManager.LoadScene("lvl_02");
    }
    public void NextLevel()
    {
        SceneManager.LoadScene("lvl_03");
    }
    IEnumerator IE_anim()
    {
        yield return new WaitForSeconds(6.5f);
        animC.Play("lvl_02", -1, 0f);
        StartCoroutine(IE_anim());
    }
    public void OpenHELP()
    {
        if (!p_help.activeInHierarchy)
        {
            p_help.SetActive(true);
        }
        else
        {
            p_help.SetActive(false);
        }
    }
}
