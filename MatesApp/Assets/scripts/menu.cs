﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class menu : MonoBehaviour {

    public GameObject p_help;

    public void NextLevel()
    {
        SceneManager.LoadScene("lvl_01");
    }
    public void OpenHELP()
    {
        if (!p_help.activeInHierarchy)
        {
            p_help.SetActive(true);
        }
        else
        {
            p_help.SetActive(false);
        }
    }
}
