﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class lvl_01 : MonoBehaviour {
    public GameObject ball,ball_2;
    [SerializeField]
    Text respostaA, respostab;
    [SerializeField]
    Image gool;
    public Animator animgood,animbad;
    public GameObject restart, nextlevel;
    public GameObject panel,p_help;

    private float temps,alturamax,velocitat, v_final, v_inicial, gravetat=0;

    void Start()
    {
        animgood.enabled = false;
        animbad.enabled = false;
    }
    public void Comparar()
    {
        string resultatAJugador = respostaA.text;
        string resultatBJugador = respostab.text;
        Debug.Log("resJ "+resultatAJugador);
        if(resultatAJugador == CalcularAlturaMaxima() && resultatBJugador == CalcularDistanciaRecorreguda())
        {
            ball.SetActive(true);
            ball_2.SetActive(false);
            animgood.enabled = true;
            StartCoroutine(esperandoanim());
        }
        else if (resultatAJugador ==CalcularAlturaMaxima())
        {
            respostaA.GetComponentInParent<InputField>().enabled = false;
            ball.SetActive(false);
            ball_2.SetActive(true);
            animbad.enabled = true;
            StartCoroutine(esperandoanim_2());
        }
        else if (resultatBJugador == CalcularDistanciaRecorreguda())
        {
            respostab.GetComponentInParent<InputField>().enabled = false;
            ball.SetActive(false);
            ball_2.SetActive(true);
            animbad.enabled = true;
            StartCoroutine(esperandoanim_2());
        }
        else
        {
            animbad.Play("ball_2", -1, 0f);
        }
    }

    IEnumerator esperandoanim()
    {
        yield return new WaitForSeconds(3.5f);
        gool.enabled = true;
        panel.SetActive(true);
        nextlevel.SetActive(true);
    }
    IEnumerator esperandoanim_2()
    {
        yield return new WaitForSeconds(3.5f);
        ball.SetActive(true);
    }
    string CalcularAlturaMaxima()
    {
        velocitat = 20;
        gravetat = 9.8f;
        v_final = 0;
        v_inicial = (Mathf.Sin(37*Mathf.Deg2Rad))*velocitat;
        temps = (v_final - v_inicial) / gravetat;
        temps = -(temps);
        Debug.Log("temps "+temps);
        alturamax = (v_inicial * temps)/2;
        double result_AM = System.Math.Round(alturamax, 1);
        Debug.Log("alturamax "+result_AM);
        return result_AM.ToString();
    }
    string CalcularDistanciaRecorreguda()
    {
        float x= (Mathf.Cos(37 * Mathf.Deg2Rad)) * velocitat;
        x = (x * temps)*2;
        float vf = -9.8f + v_inicial;
        double Result_DR = System.Math.Round(vf, 1);
        Debug.Log("vf " + Result_DR);
        return Result_DR.ToString();
    }

    public void Restart()
    {
        SceneManager.LoadScene("lvl_01");
    }
    public void NextLevel()
    {
        SceneManager.LoadScene("lvl_02");
    }
    public void OpenHELP()
    {
        if(!p_help.activeInHierarchy)
        {
            p_help.SetActive(true);
        }
        else
        {
            p_help.SetActive(false);
        }
    }
}
