﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class lvl_04 : MonoBehaviour {

    [SerializeField]
    Text respostaA, respostaB;
    public GameObject restart, nextlevel;
    public GameObject block_screen, p_help;
    private float gravetat, velocitat_a,velocitat_b,altura_b,altura_c;
    [SerializeField]
    Animator animC;

    void Start()
    {
        gravetat = 9.8f;
        velocitat_a = 20;
        velocitat_b = 6;
        CalculaAltura();
        CalculaB();
        StartCoroutine(IE_anim());
    }

    public void Comparar()
    {
        string resultatAJugador = respostaA.text;
        string resultatBJugador = respostaB.text;

        if (resultatAJugador == CalculaAltura() && resultatBJugador == CalculaB())
        {
            block_screen.SetActive(true);
            nextlevel.SetActive(true);
        }
        else if (resultatAJugador == CalculaAltura())
        {
            Debug.Log("A GOOD");
            respostaA.GetComponentInParent<InputField>().enabled = false;
        }
        else if (resultatBJugador == CalculaB())
        {
            Debug.Log("B GOOD");
            respostaB.GetComponentInParent<InputField>().enabled = false;
        }

    }

    string CalculaAltura()
    {
        //ECa+EPa=ECb+EPb
        //0.5*MVA2=m*g*H
        float Ea;
        Ea = 0.5f * (velocitat_a * velocitat_a);
        altura_b = Ea / gravetat;
        double respostaA = System.Math.Round(altura_b, 1);
        Debug.Log("respostaA " + respostaA);
        return respostaA.ToString();
    }
    string CalculaB()
    {
        float Ea, Eb;
        Ea = 0.5f * (velocitat_a * velocitat_a);
        Eb = 0.5f * (velocitat_b * velocitat_b);
        altura_c = (Ea - Eb) / gravetat;
        double respostaB = System.Math.Round(altura_c, 1);
        Debug.Log("respostaB " + respostaB);
        return respostaB.ToString();
    }
    public void Restart()
    {
        SceneManager.LoadScene("lvl_04");
    }
    public void NextLevel()
    {
        SceneManager.LoadScene("lvl_05");
    }
    IEnumerator IE_anim()
    {
        yield return new WaitForSeconds(6.5f);
        animC.Play("lvl_04", -1, 0f);
        StartCoroutine(IE_anim());
    }
    public void OpenHELP()
    {
        if (!p_help.activeInHierarchy)
        {
            p_help.SetActive(true);
        }
        else
        {
            p_help.SetActive(false);
        }
    }
}
